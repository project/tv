<?php

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\UserDataInterface;

function tv_form_user_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
    $user = \Drupal::currentUser();
    $uid = $user->id();
    /** @var UserDataInterface $userData */
    $userData = \Drupal::service('user.data');

    $form['tv'] = [
        '#type' => 'details',
        '#title' => t('TV settings'),
        '#open' => TRUE,
        'autoplay' => [
            '#type' => 'checkbox',
            '#title' => t('Autoplay'),
            '#description' => t('Whether to auto-play TV channels.'),
            '#default_value' => $userData->get('tv', $uid, 'autoplay'),
        ],
        'mute' => [
            '#type' => 'checkbox',
            '#title' => t('Mute'),
            '#description' => t('Whether to auto-mute TV channels.'),
            '#default_value' => $userData->get('tv', $uid, 'mute'),
        ],
    ];

    $form['actions']['submit']['#submit'][] = 'tv_user_form_submit';
}

function tv_user_form_submit($form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    $uid = $user->id();
    /** @var UserDataInterface $userData */
    $userData = \Drupal::service('user.data');
    $values = $form_state->getValues();
    $userData->set('tv', $uid, 'autoplay', $values['autoplay']);
    $userData->set('tv', $uid, 'mute', $values['mute']);
}

function tv_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
    if ($form_id === 'node_tv_channel_form' || $form_id === 'node_tv_channel_edit_form') {
        $form['field_tags']['widget']['target_id']['#description'] = new TranslatableMarkup('<a href="@overview" target="_blank">Tags</a> videos must have to appear on this TV Channel.', [
            '@overview' => '/admin/structure/taxonomy/manage/tags/overview',
        ]);
    }
}

# TV Module
TV provides TV Apps, TV Channel entities & supporting infrastructure.

Each TV media entity is an application that works like a modern "Smart TV" & can run full-screen. Each TV media entity application can be controlled using standard-fare media center hotkeys.

## Installation
Install the module as you would any other Drupal module.

The module will create:
* a "TV Channel" content type
* a "TV" Block

The module will install [remote_video](https://www.drupal.org/project/remote_video) as a dependency. Visit `/admin/config/services/remote_video` to add your video service API key(s).

### Start the React app
If running on a development machine, you'll need to install & start the React TV app.

1. `cd web/modules/custom/tv/js`
2. `npm install`
3. `npm start`

## Configuration
Create a TV Channel. Or several.

> A TV Channel is a dynamic collection of media that is often played in a linear fashion. A TV Channel can be tagged with taxonomy terms. A TV Channel will play relevant tagged media. TV Channel media may also be affected by the current user's context & brand-new relevant media.

### Create a "Drupal DriesNotes" Sample TV Channel

1. Visit `/admin/structure/taxonomy/manage/tags/add`
2. Create "Drupal", "Keynote" & "Dries" tags via that form.
3. Visit `/media/add/remote_video`.
4. Set `Video URL` to [Driesnote: Lille DrupalCon 2023](https://www.youtube.com/watch?v=tFxlDBLkLJc).
5. Set `Tags` to "Drupal", "Keynote" & "Dries".
6. Save the form.
7. Repeat steps 4-6 for [Driesnote: Pittsburgh DrupalCon 2023](https://www.youtube.com/watch?v=tNa4XKb3zds).
8. Repeat steps 4-6 for [Driesnote: Portland DrupalCon 2022](https://www.youtube.com/watch?v=Ig676RzJbLo).
9. Visit `/admin/structure/block` & place a "TV" block where you would prefer (e.g. the "Content" region on "TV Channel" pages).
10. Visit `/node/add/tv_channel` to create a "Drupal Keynotes" TV Channel.
11. Set the TV Channel title to "Drupal DriesNotes".
12. Set the TV Channel tags to "Drupal", "Keynote" & "Dries".
13. Submit the form to create the TV Channel.
14. Scroll down to view your new TV Channel.

Repeat the above to create more TV Channels.

## Programmatic Customization
TV Channel item lists can be modified by implementing `hook_tv_channel_items_alter`:

```php
/**
 * Implements hook_tv_channel_items_alter().
 */
function mymodule_tv_channel_items_alter(array &$items, array $context) {
    $preroll = [
        'name' => 'Thanks for watching!',
        'url' => '', // Video URL
        // ...
    ];
    array_unshift($items, $preroll);
}
```

## Usage
Each TV has two main modes of operation:
* "Channel" mode: the TV plays a Channel (e.g. category) of tagged media in a linear fashion, avoiding media that has been played for the user recently. The user can Play/Pause, change Channels, and view a Channel list with their computer remote.
* "Library" mode: the TV shows a media library of curated media as options for the user. Once the chosen media is done playing, the Library is shown again -- with the summary of their chosen media highlighted.

Content editors are able to embed a TV anywhere other media can be embedded. A TV can be configured to be context-aware. For instance, a relevant TV Channel can automatically be chosen based on the page the TV is embedded on. A TV can also be configured to ignore relevant options if they are deemed to be "expired" (e.g. older than N years). A TV can be configured to avoid playing promotional videos (and play premium videos) based on the current user's subscription status.

### Typical Use Cases
#### Busy Drupal Developer
Consider the case of a busy Drupal developer hoping to watch videos about the Automated Updates Strategic Initiative while starting laundry, making dinner, eating, washing dishes & folding clothes. The user could -- first -- navigate to the project page for that initiative & click Play (if that page has a TV embedded). The most recent relevant videos in the user's preferred language would play first. Re-runs would seldom occur & old videos wouldn't appear. The user is able to learn the latest about the initiative without having to routinely queue up a new video. When they navigate back to the page the following week, they can easily pick up where they left off.

<?php

namespace Drupal\tv\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\remote_video\Services\RemoteVideoConfigServiceInterface;
use Drupal\tv\Entity\Collection\TvChannelCollection;
use Drupal\tv\Entity\TvChannel;
use Drupal\tv\Service\TvChannelsServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Settings extends ConfigFormBase {

    private RemoteVideoConfigServiceInterface $remoteVideoConfig;
    private TvChannelsServiceInterface $channelsService;
    private EntityTypeManagerInterface $entityTypeManager;

    public function __construct(ConfigFactoryInterface            $config_factory,
                                EntityTypeManagerInterface        $entityTypeManager,
                                RemoteVideoConfigServiceInterface $remoteVideoConfig,
                                TvChannelsServiceInterface        $channelsService)
    {
        parent::__construct($config_factory);
        $this->entityTypeManager = $entityTypeManager;
        $this->remoteVideoConfig = $remoteVideoConfig;
        $this->channelsService = $channelsService;
    }

    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('config.factory'),
            $container->get('entity_type.manager'),
            $container->get('remote_video.config'),
            $container->get('tv.channels')
        );
    }

    public function getFormId() {
        return 'tv_settings';
    }

    protected function getEditableConfigNames() {
        return [];
    }

    public function buildForm(array $form, FormStateInterface $form_state): array
    {
        $report = $this->getReport();
        foreach(['error', 'warning'] as $key) {
            if (isset($report[$key])) {
                foreach ($report[$key] as $message) {
                    $this->messenger()->addMessage($message, $key);
                }
            }
        }

        $form['channels'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Channels'),
        ];
        $form['channels']['items'] = [
            '#type' => 'tableselect',
            '#header' => $this->getChannelsHeader(),
            '#options' => $this->getChannelsOptions(),
            '#empty' => new TranslatableMarkup('No channels found. Click <a href="@url" target="_blank">here</a> to add a channel.', [
                '@url' => '/node/add/tv_channel'
            ]),
            '#disabled' => TRUE,
        ];
        return $form;
    }

    private function getChannels(): TvChannelCollection
    {
        return $this->channelsService->getChannels();
    }

    private function getChannelsHeader(): array
    {
        return [
            'channel' => $this->t('Channel'),
            'published' => $this->t('Published'),
            'videos' => $this->t('Videos'),
            'likes' => $this->t('Likes'),
            'rating_avg' => $this->t('Rating Average'),
            'subscribers' => $this->t('Subscribers'),
        ];
    }

    private function getChannelsOptions(): array
    {
        $options = [];
        foreach ($this->getChannels() as $channel) {
            /** @var TvChannel $channel */
            $options[$channel->id()] = [
                'channel' => $channel->getTitle(),
                'published' => $channel->isPublished() ? 'Yes' : 'No',
                'videos' => $channel->getVideoCount(),
                'likes' => $channel->getLikeCount(),
                'rating_avg' => $channel->getRatingAverage(),
                'subscribers' => $channel->getSubscriberCount(),
            ];
        }
        return $options;
    }

    private function isExtended(string $provider): bool
    {
        $apiProviderModule = 'remote_video_' . strtolower($provider);
        return \Drupal::moduleHandler()->moduleExists($apiProviderModule);
    }

    private function isConfigured(string $provider): bool
    {
        if (!$this->isExtended($provider)) {
            return FALSE;
        }

        return $this->remoteVideoConfig->getRemoteVideoService($provider)?->isConfigured() ?? FALSE;
    }

    private function getReport(): array
    {
        $report = [];
        $mediaType = $this->entityTypeManager->getStorage('media_type')->load('remote_video');
        $providers = $mediaType->getSource()->getProviders() ?? [];
        foreach ($providers as $provider) {
            $isExtended = $this->isExtended($provider);
            if ($isExtended) {
                $isConfigured = $this->isConfigured($provider);
                if (!$isConfigured) {
                    $report['error'][] = $this->t('@provider provider enabled & extended, but is not configured. You must configure it <a href="@url" target="_blank">here</a> to support browsing of Remote Video entities in TV instances.', [
                        '@provider' => $provider,
                        '@url' => '/admin/config/services/remote_video',
                    ]);
                }
            }
            else {
                $module = 'remote_video_' . strtolower($provider);
                $report['warning'][] = $this->t('@provider provider enabled, but not extended. Consider disabling the provider <a href="@providerConfigUrl" target="_blank">here</a>, or <a href="@extendUrl">extending</a> it by installing the <a href="@moduleUrl" target="_blank">@module</a> module.', [
                    '@providerConfigUrl' => '/admin/structure/media/manage/remote_video',
                    '@provider' => $provider,
                    '@extendUrl' => '/admin/modules',
                    '@moduleUrl' => 'https://www.drupal.org/project/' . $module,
                    '@module' => $module,
                ]);
            }
        }
        return $report;
    }

}

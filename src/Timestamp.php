<?php

namespace Drupal\tv;

use DateTime;

class Timestamp
{
    private DateTime $timestamp;

    public function __construct($hour = 0, $minute = 0, $second = 0)
    {
        $this->timestamp = new DateTime();
        $this->timestamp->setTime($hour, $minute, $second);
    }

    public function __toString(): string
    {
        return $this->timestamp->format('H:i:s');
    }
}

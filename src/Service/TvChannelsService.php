<?php

namespace Drupal\tv\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\tv\Entity\Collection\TvChannelCollection;

class TvChannelsService implements TvChannelsServiceInterface {
    private EntityTypeManagerInterface $entityTypeManager;

    public function __construct(EntityTypeManagerInterface $entityTypeManager)
    {
        $this->entityTypeManager = $entityTypeManager;
    }

    public function getChannels(): TvChannelCollection {
        return (new TvChannelCollection($this->entityTypeManager))->load();
    }
}

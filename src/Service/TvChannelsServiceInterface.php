<?php

namespace Drupal\tv\Service;

use Drupal\tv\Entity\Collection\TvChannelCollection;

interface TvChannelsServiceInterface {
    public function getChannels(): TvChannelCollection;
}

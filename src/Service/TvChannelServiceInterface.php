<?php

namespace Drupal\tv\Service;

use Drupal\node\NodeInterface;

interface TvChannelServiceInterface {
    public function getItems(NodeInterface $node): array;

    public function getAutoPlayPreference(): bool;

    public function getMutePreference(): bool;
}

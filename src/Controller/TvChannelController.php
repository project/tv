<?php

namespace Drupal\tv\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\node\NodeInterface;
use Drupal\tv\Service\TvChannelServiceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns responses for Node routes.
 */
class TvChannelController extends ControllerBase implements ContainerInjectionInterface {

    public function getChannel(NodeInterface $node): JsonResponse
    {
        /** @var TvChannelServiceInterface $channelService */
        $channelService = \Drupal::service('tv.channel');
        return new JsonResponse([
            'title' => $node->getTitle(),
            'method' => 'GET',
            'status'=> 200,
            'autoplay' => $channelService->getAutoPlayPreference(),
            'mute' => $channelService->getMutePreference(),
            'playlist' => $channelService->getItems($node),
        ], 200);
    }

}

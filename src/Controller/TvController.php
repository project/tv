<?php

namespace Drupal\tv\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns responses for Node routes.
 */
class TvController extends ControllerBase implements ContainerInjectionInterface {
    public function getChannels(): JsonResponse
    {
        $items = $this->getItems();
        return new JsonResponse([
            'title' => 'TV Channels',
            'method' => 'GET',
            'status'=> JsonResponse::HTTP_OK,
            'channels' => $items,
        ], JsonResponse::HTTP_OK);
    }

    private function getItems(): array
    {
        $channels = [];

        // Load all tv_channel nodes.
        $query = \Drupal::entityQuery('node')
            ->condition('type', 'tv_channel')
            ->condition('status', 1)
            ->sort('title')
            ->accessCheck();

        foreach ($query->execute() as $nid) {
            $node = \Drupal\node\Entity\Node::load($nid);
            $channels[] = [
                'id' => (int) $node->id(),
                'title' => $node->getTitle(),
                // @todo 'posterUrl' => $node->field_poster->entity->getFileUri(),
                'playlist' => $node->toUrl()->toString() . '/playlist',
            ];
        }
        return $channels;
    }

}

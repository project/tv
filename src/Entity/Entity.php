<?php

namespace Drupal\tv\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

abstract class Entity {

    protected string $type;
    protected string $bundle;

    protected EntityTypeManagerInterface $entityTypeManager;
    protected EntityInterface $entity;

    public function __construct(EntityTypeManagerInterface $entityTypeManager)
    {
        $this->entityTypeManager = $entityTypeManager;
    }

    public function load(int $id): static
    {
        $this->entity = $this->entityTypeManager->getStorage('node')->load($id);
        if ($this->entity->bundle() !== $this->bundle) {
            throw new \LogicException(vsprintf('Cannot load %s into %s', [
                $this->bundle,
                static::class
            ]));
        }
        return $this;
    }

    public function id(): int
    {
        return $this->entity->id();
    }

    public function getTitle(): string
    {
        return $this->entity->getTitle();
    }

    public function isPublished(): bool
    {
        return $this->entity->isPublished();
    }
}

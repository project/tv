<?php

namespace Drupal\tv\Entity\Collection;

use ArrayAccess;
use ArrayIterator;
use Countable;
use Drupal\tv\Entity\Entity;
use SeekableIterator;
use Serializable;
use Traversable;

abstract class EntityCollection implements SeekableIterator, ArrayAccess, Serializable, Countable {

    private ArrayIterator $iterator;
    protected array $data = [];

    public function append($item)
    {
        $this->data[] = $item;
    }

    public function getIterator(): Traversable
    {
        if (!isset($this->iterator)) {
            $this->iterator = new ArrayIterator($this->data);
        }
        return $this->iterator;
    }

    public function offsetExists(mixed $offset): bool
    {
        return isset($this->data[$offset]);
    }

    public function offsetGet(mixed $offset): mixed
    {
        return $this->data[$offset];
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->data[$offset] = $value;
    }

    public function offsetUnset(mixed $offset): void
    {
        unset($this->data[$offset]);
    }


    public function count(): int
    {
        return count($this->data);
    }

    public function serialize(): string
    {
        return serialize($this->data);
    }

    public function unserialize(string $data)
    {
        $this->data = unserialize($data);
    }

    public function __serialize(): array
    {
        return $this->data;
    }

    public function __unserialize(array $data): void
    {
        $this->data = $data;
    }

    abstract public function current(): Entity;

    public function next(): void
    {
        $this->getIterator()->next();
    }

    public function key(): mixed
    {
        return $this->getIterator()->key();
    }

    public function valid(): bool
    {
        return $this->getIterator()->valid();
    }

    public function rewind(): void
    {
        $this->getIterator()->rewind();
    }

    public function seek(int $offset): void
    {
        $this->getIterator()->seek($offset);
    }

}

<?php

namespace Drupal\tv\Entity\Collection;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\tv\Entity\TvChannel;

class TvChannelCollection extends EntityCollection {

    private EntityTypeManagerInterface $entityTypeManager;

    public function __construct(EntityTypeManagerInterface $entityTypeManager)
    {
        $this->entityTypeManager = $entityTypeManager;
    }

    public function load(): static
    {
        $result = $this->entityTypeManager->getStorage('node')
            ->getQuery()
            ->condition('type', 'tv_channel')
            ->sort('title')
            ->accessCheck()
            ->execute();
        foreach ($result as $nid) {
            $this->append($nid);
        }
        return $this;
    }

    public function current(): TvChannel
    {
        return (new TvChannel($this->entityTypeManager))
            ->load(current($this->data));
    }
}

<?php

namespace Drupal\tv\Entity;

class TvChannel extends Entity {
    protected string $type = 'node';
    protected string $bundle = 'tv_channel';

    public function getSubscriberCount(): int
    {
        // @todo return the number of users who have subscribed to this channel.
        return 0;
    }

    public function getRatingAverage(): float
    {
        // @todo return the average rating of the channel.
        return 0;
    }

    public function getVideos(): array
    {
        // @todo return the videos of the channel.
        return [];
    }

    public function getVideoCount(): int
    {
        return count($this->getVideos());
    }

    public function getLikeCount(): int
    {
        // @todo return the number of likes of the channel.
        return 0;
    }
}

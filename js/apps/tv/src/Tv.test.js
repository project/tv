import { render, screen } from '@testing-library/react';
import Tv from './Tv';

test('renders learn react link', () => {
  render(<Tv />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

function getInSeconds(timestamp) {
    let a = timestamp.split(':');
    return (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
}

export {getInSeconds};

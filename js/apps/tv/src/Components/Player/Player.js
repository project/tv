import YouTube from "./Embed/YouTube/YouTube";

function Player(params) {
    let item = 0;

    function getPlatform(url) {
        if (url.includes('youtube.com')) {
            return 'youtube';
        }
    }

    if (getPlatform(params.channel.playlist[item].url) === 'youtube') {
        return (
            <div className="Player">
                <YouTube channel={params.channel} item={item}/>
            </div>
        );
    }
}

export default Player;

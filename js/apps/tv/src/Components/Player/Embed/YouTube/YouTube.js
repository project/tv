import {getInSeconds} from "../../../../helpers";

function YouTube(params) {
    let channel = params.channel;
    let video = channel.playlist[params.item];

    function getVideoId(url) {
        const videoId = url.split('v=')[1];
        const ampersandPosition = videoId.indexOf('&');
        if (ampersandPosition !== -1) {
            return videoId.substring(0, ampersandPosition);
        }
        return videoId;
    }

    let urlParams = new URLSearchParams();
    urlParams.append('rel', 0);
    urlParams.append('fs', 0);
    urlParams.append('disablekb', 1);
    urlParams.append('autoplay', channel.autoplay);
    urlParams.append('mute', channel.mute);
    urlParams.append('start', getInSeconds(video.startTs));
    urlParams.append('cc_load_policy', channel.closedCaptions ?? 0);
    urlParams.append('iv_load_policy', channel.annotations ?? 3);
    urlParams.append('enablejsapi', 1);
    const src = 'https://www.youtube.com/embed/' +
        getVideoId(video.url) +
        '?' + urlParams.toString();

    // Load the YouTube API if not already loaded.
    if (!document.getElementById('iframe-demo')) {
        let tag = document.createElement('script');
        tag.id = 'iframe-demo';
        tag.src = 'https://www.youtube.com/iframe_api';
        let firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }

    // Configure the YouTube API & TV Player embeds.
    window.onYouTubeIframeAPIReady = function () {
        // Attach the API to each player.
        const tvs = document.getElementsByClassName('tv');
        for (let tv of tvs) {
            let embed = tv.getElementsByClassName('youtube')[0];

            tv.players = tv.players || {};
            tv.players.youtube = new YT.Player(embed);

            // Provide standardized player interface.
            tv.players.youtube.play = function () {
                tv.players.youtube.playVideo();
            };
            tv.players.youtube.pause = function () {
                tv.players.youtube.pauseVideo();
            };
            tv.players.youtube.load = function (url) {
                const videoId = getVideoId(url);
                tv.players.youtube.loadVideoById(videoId);
            };
            tv.players.youtube.playPause = function () {
                if (tv.players.youtube.getPlayerState() === 1) {
                    tv.players.youtube.pause();
                } else {
                    tv.players.youtube.play();
                }
            }

            // Set the default player to be the YouTube player.
            tv.defaultPlayer = tv.players.youtube;
        }
    }

    return (
        <iframe className="youtube" width="100%" height="100%" src={src}
                title="YouTube video player" frameBorder="0"
                allow="autoplay"
                allowFullScreen></iframe>
    );
}

export default YouTube;

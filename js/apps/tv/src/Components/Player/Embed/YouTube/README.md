# YouTube Video Embed
This [component](YouTube.js) is used to embed a YouTube video into the [Player](../../Player.js).

## Reference
- [YouTube Player Embedding](https://developers.google.com/youtube/player_parameters)
- [YouTube Player Parameters](https://developers.google.com/youtube/player_parameters)
- [YouTube Player API](https://developers.google.com/youtube/iframe_api_reference)

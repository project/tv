function Controls(params) {

    function toggleControls() {
        const controls = document.querySelector('.Controls');
        controls.classList.toggle('active');
    }

    function hideControls() {
        const controls = document.querySelector('.Controls');
        controls.classList.remove('active');
    }

    function playPause() {
        params.root.defaultPlayer.playPause();
    }

    function next() {
        // @todo implement function
        console.log('next');
    }

    function previous() {
        // @todo implement function
        console.log('previous');
    }

    function nextChannel() {
        // @todo implement function
        console.log('next channel');
    }

    function previousChannel() {
        // @todo implement function
        console.log('previous channel');
    }

    function fullscreen() {
        const body = document.getElementsByTagName('body')[0];
        const tv = params.root.getElementsByClassName('Tv')[0];
        if (body.classList.contains('fullscreen')) {
            body.classList.remove('fullscreen');
            tv.classList.remove('fullscreen');
            closeFullscreen();
        } else {
            body.classList.add('fullscreen');
            tv.classList.add('fullscreen');
            openFullscreen();
            hideControls();
        }
    }

    function openFullscreen() {
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement.webkitRequestFullscreen) { /* Safari */
            document.documentElement.webkitRequestFullscreen();
        } else if (document.documentElement.msRequestFullscreen) { /* IE11 */
            document.documentElement.msRequestFullscreen();
        }
    }

    function closeFullscreen() {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.webkitExitFullscreen) { /* Safari */
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) { /* IE11 */
            document.msExitFullscreen();
        }
    }

    return (
        <div className="Controls">
            <button onClick={toggleControls}>Controls</button>
            <div>
                <div>
                    <button onClick={playPause}>Play/Pause</button>
                </div>
                <div>
                    <button onClick={next}>Next</button>
                    <button onClick={previous}>Previous</button>
                </div>
                <div>
                    <button onClick={nextChannel}>Next Channel</button>
                    <button onClick={previousChannel}>Previous Channel</button>
                </div>
                <div>
                    <button onClick={fullscreen}>Full Screen</button>
                </div>
            </div>
        </div>
    );
}

export default Controls;

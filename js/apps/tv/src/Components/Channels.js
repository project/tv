function Channels(params) {
    function toggleChannels() {
        const channel = document.querySelector('.Channels');
        channel.classList.toggle('active');
    }

    function loadChannel(item) {
        return function () {
            // @todo implement function
            console.log(item);
        }
    }

    return (
        <div className="Channels">
            <button onClick={toggleChannels}>Channels</button>
            <h3>Channels</h3>
            <div className="scrollable">
                <ul>
                    {params.channels.map((item, index) => (
                        <li key={item.id} className={item.id == params.current ? 'current' : ''}>
                            <a onClick={loadChannel(item)}>{item.title}</a>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
}

export default Channels;

function Channel(params) {
    function toggleChannel() {
        const channel = document.querySelector('.Channel');
        channel.classList.toggle('active');
    }

    function hideChannel() {
        const channel = document.querySelector('.Channel');
        channel.classList.remove('active');
    }

    function loadItem(item) {
        return function () {
            params.root.defaultPlayer.load(item.url);
            hideChannel();
        }
    }

    return (
        <div className="Channel">
            <button onClick={toggleChannel}>Channel</button>
            <h3>{params.channel.title} Channel</h3>
            <div className="scrollable">
                <ul>
                    {params.channel.playlist.map((item, index) => (
                        <li key={item.id} className={index === params.current ? 'current' : ''}>
                            <a onClick={loadItem(item)}><img src={item.posterUrl} loading="lazy"
                                                             alt="Poster"/>{item.name}</a>
                            <p>Duration: {item.duration}</p>
                            <p>Progress: {item.progress}%</p>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
}

export default Channel;

import React, {useEffect, useState} from 'react';
import './Tv.css';
import Channel from "./Components/Channel";
import Player from "./Components/Player/Player";
import Controls from "./Components/Controls";
import Ticker from "./Components/Ticker";
import Channels from "./Components/Channels";

function Tv(params) {
    const [channel, setChannel] = useState([]);
    const [channels, setChannels] = useState([]);

    useEffect(() => {
        getChannel();
        getChannels();
    }, []);

    async function getChannel() {
        const response = await fetch(params.baseUrl + '/node/' + params.channel + '/channel');
        const channel = await response.json();
        setChannel(channel);
    }

    async function getChannels() {
        const response = await fetch(params.baseUrl + '/tv/channels');
        const channelsData = await response.json();
        setChannels(channelsData.channels);
    }

    // @todo use state API to determine which menu is active
    function isChannelMenuActive() {
        return params.root.querySelector('.Channel').classList.contains('active');
    }
    function isChannelsMenuActive() {
        return params.root.querySelector('.Channels').classList.contains('active');
    }
    function isControlsMenuActive() {
        return params.root.querySelector('.Controls').classList.contains('active');
    }
    function getActiveMenu() {
        if (isChannelMenuActive()) {
            return params.root.querySelector('.Channel');
        }
        if (isChannelsMenuActive()) {
            return params.root.querySelector('.Channels');
        }
        if (isControlsMenuActive()) {
            return params.root.querySelector('.Controls');
        }
        return null;
    }

    if (typeof channel.title === 'undefined') {
        return null;
    }

    document.addEventListener('keyup', (e) => {
        e.preventDefault();
        const channel = params.root.querySelector('.Channel');
        const channels = params.root.querySelector('.Channels');
        const controls = params.root.querySelector('.Controls');

        const menu = getActiveMenu();
        if (typeof menu !== 'undefined' && menu !== null) {
            console.log(menu.classList);
        }
        switch (e.key) {
            case ' ':
                params.root.defaultPlayer.playPause();
                break;
            case 'ArrowRight':
                if (menu) {
                    // @todo hide the menu if it is on the right edge & do not show the left menu
                } else {
                    // @todo trigger Channel menu active state instead of toggling classes below
                    controls.classList.remove('active');
                    channels.classList.remove('active');
                    channel.classList.add('active');
                }
                break;
            case 'ArrowLeft':
                if (menu) {
                    // @todo hide the menu if it is on the left edge & do not show the right menu
                } else {
                    // @todo trigger Channels menu active state instead of toggling classes below
                    controls.classList.remove('active');
                    channel.classList.remove('active');
                    channels.classList.add('active');
                }
                break;
            case 'ArrowDown':
                if (menu === channel || menu === channels) {
                    // @todo highlight next option
                } else {
                    // @todo trigger Search menu state instead of toggling classes below
                    // @todo implement search
                    channels.classList.remove('active');
                    channel.classList.remove('active');
                    controls.classList.remove('active');
                }
                break;
            case 'ArrowUp':
                if (menu === channel || menu === channels) {
                    // @todo highlight previous option
                } else {
                    // @todo trigger Controls menu state instead of toggling classes below
                    // @todo implement search
                    channels.classList.remove('active');
                    channel.classList.remove('active');
                    controls.classList.add('active');
                }
                break;
            case 'Enter':
                if (menu) {
                    // @todo set the selected option to current & load it
                }
                break;
        }
    });

    return (
        <div className="Tv">
            <Player channel={channel}/>
            {/* @todo <Search /> */}
            <Controls root={params.root}/>
            {/* @todo <Ticker /> */}
            <Channel root={params.root} channel={channel} current={0}/>
            <Channels channels={channels} current={params.channel}/>
        </div>
    );
}

export default Tv;
